---

name: "Prompt Submission"
about: "Submit a writing prompt to our collection"
labels:
- submission

---

Fill out this template to submit your writing prompts.

## Prompts

Below, list as many (or as few) writing prompts as you would like:



## Contributor Information

We need this information so we can properly give you credit for your submission. Be aware that people using your prompts have no legal obligation to attribute them to you, but Freedom to Write will use this information whenever possible.

**Full Name or Psuedonym:** 

**Link to Personal Site or Profile:** 

_Note: You can link to any personal website, blog, or social media profile, as long as it is appropriate for a wide audience, potentially including minors. Any site you submit will be reviewed by Freedom to Write and must meet our approval to be used._

**Short Bio (ideally <= 64 words):**



_Note: Again, use of your bio is subject to our approval._

## Legal

By submitting contributions to this repository, you are licensing them into the Public Domain under [CC0 1.0 International](https://creativecommons.org/publicdomain/zero/1.0/legalcode). No one who uses your submissions has any legal obligation to give you any credit, and there will be little to no restriction on how or why your work may be used. To agree to this, please copy the following sentence below: **I agree that all of my work submitted here may be used by anyone for any purpose without restriction under the terms of CC0 1.0 International.**


