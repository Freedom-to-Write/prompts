# Open Prompts

An open yet curated list of public domain writing prompts.

## Contributing Prompts

If you would like to submit a prompt, you're very welcome to do so! [Submit an issue](https://codeberg.org/Freedom-to-Write/prompts/issues/new?template=.gitea%2Fissue_template%2Fprompt_submission.md) to this repository and we'll begin the process of introducing them into the data in this repository. You will need a Codeberg account for this.

In the future, we hope to have a separate submission form to make the process more accessible, but the project is currently in its early stages.

You may submit as many prompts as you like in a single issue. It would also help if you include information about yourself including name (or pseudonym), a link to a personal site or profile, and a short (~50 word) bio.

**Be aware that by submitting to this project you are releasing your contributions into the public domain under [CC0 1.0 International](https://creativecommons.org/publicdomain/zero/1.0/), where anyone may use them for any purpose without asking your permission or giving you credit.** Freedom to Write plans to credit you with at least your name and the link you provide whenever we use your work, but we have no legal responsibility to do so, and neither does anyone else.

## Repository Structure

This repository contains the prompt data, which is available for use in any project that needs writing prompts. Freedom to Write is working on coding a website as a searchable index and with an API for use in other projects.

The data is stored in [TOML](https://toml.io) configuration files.

### prompts.toml

This file contains the actual writing prompts. It is stored as an [array of tables](https://toml.io/en/v1.0.0#array-of-tables) with the key `prompts`.

#### `id`

A unique integer id of the writing prompt.

This should be incremented for every new prompt; even if a prompt is deleted, its id should not be reused.

In the case of a prompt's translation into another language, it should be a new entry in prompts.toml, but *with the same `id` field*. This is to identify it as the same prompt, but translated. Note that the `language` field *will* be different.

This means that, technically, `id` is not unique. If you need a guaranteed unique identifier, combine the `id` and `language` fields as `[id]-[language]`. For example, `3-en-US` represents the US English translation of prompt #3.

#### `text`

The full text of the writing prompt, as a string, in the language specified by `language`.

#### `contributor`

The key of a contributor from the `contributors.toml` file, as a string. This represents the contributor who *originally submitted* the prompt and does not necessarily reflect future editing.

#### `published`

The ISO date (not including time) when the prompt was first added to the repository. (yyyy-mm-dd)

This is the date that the prompt is added to the files, not necessarily the date it reaches the main branch; if you are adding prompts to the repository you may set this to the current date, even if this is not the date the changes go live.

#### `updated`

The ISO date (not including time) when the prompt was last updated. (yyyy-mm-dd)

#### `language`

The IETF language tag representing the language of the prompt. For example, `en-US`. Only the primary language subtag is required.

Note that writing prompts in languages other than `en` may take extra time to approve, at this time.

Note that when filtering for a language, you likely will only want to pay attention to the primary language subtag; in the case of `en-US`, this is `en`.

To translate a writing prompt, duplicate it, keeping the same `id`, and update `language` and `text`. Optionally, you may fill the `translator` field with your contributor id (see `contributors.toml`) if you are the original translator of the prompt to the target language.

#### `translator`

Optional, represents the contributor id of the original translator of the writing prompt into a new target language.

#### `tags`

An array of strings, each of which is a tag for the prompt. In the future, this repo may have a `tags.toml` file with metadata about each tag, but it does not currently.

### contributors.toml

This file contains a list of all the people who have contributed to this project, including people submitting prompts, editing prompts, and providing code. Note that editors and coders will not be given credit on individual prompts.

#### `name`

The full name (or preferred pseudonym) of the contributor.

#### `link`

A link to a webpage about or by the contributor. This could be a personal website, social media profile, Codeberg or other git hosting service profile, or any other link of the contributor's choice.

For this project, please avoid using a link to a site with NSFW content.

#### `bio`

A short bio of the contributor; this should be appropriate for general audiences, but may be as quirky or serious as the contributor wishes. While there is no hard length limit, try to keep the bio to 64 words or under and be aware that all contributor details including the bio are subject to maintainer approval.

#### `roles`

An array of strings, each of which represents a role of the contributor. Currently, the roles are:

- **writer** – Someone contributing full writing prompts. Writers are also credited on the individual prompts.
- **editor** – Someone who helps edit and curate submitted writing prompts.
- **coder** – Someone who helps provide code for this project in any form.
- **translator** — Someone translating writing prompts into different languages. Translators can optionally be credited on individual prompts using the `translator` field. This role is also given to people reviewing translations, along with the `editor` role.

Descriptions of these roles are also stored in `roles/[language].toml`, though there is no documentation on the format in this README since it is very self-explanatory and not necessary for the vast majority of contributors. The roles subdirectory contains one file for each translation of the role descriptions.

## Tests

Tests have been written to make sure data is valid. Please run them before submitting direct changes to the data (i.e. via a pull request). To run them, first [install Deno](https://deno.land/#installation), navigate to the repository's root folder in your terminal, then run `sh tests/all.sh` to check all data.

## License

This project is public domain under [CC0 1.0 International](https://creativecommons.org/publicdomain/zero/1.0/legalcode). All contributors must release their contributions for use under this license.

This means that you can use the data in this repository for whatever you want, without citing your source or worrying about copyright violation. Be aware, though, that Freedom to Write makes no guarantee of the quality or usefulness of this project and is not liable for any harm to you through use of this project.
