#!/bin/sh

deno run --allow-read tests/roles.js
deno run --allow-read tests/contributors.js
deno run --allow-read tests/prompts.js
