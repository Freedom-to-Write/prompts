import { TOML } from "https://taisukef.github.io/toml-es/TOML.js";

(async () => {
  const raw = new TextDecoder().decode(await Deno.readFile("./contributors.toml"));
  const contributors = TOML.parse(raw);

  for (var contributor of Object.keys(contributors)) {
		// Check types
    if (typeof contributors[contributor].name !== "string") throw new Error("Contributor names must  be strings; got '" + typeof contributors[contributor].name + "' from contributor '" + contributor + "'");
    if (typeof contributors[contributor].link !== "string") throw new Error("Contributor links must be strings; got '" + typeof contributors[contributor].link + "' from contributor '" + contributor + "'");
		
		// Check if roles exist
		const raw = new TextDecoder().decode(await Deno.readFile("./roles.toml"));
		const roles = TOML.parse(raw);
		for (var role of contributors[contributor].roles) {
			if (typeof roles[role] === 'undefined') throw new Error(`Role '${role} does not exist for contributor '${contributor}'`);
		}
  }

  console.info("All contributors passed their tests!");
})();
