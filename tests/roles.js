import { TOML } from "https://taisukef.github.io/toml-es/TOML.js";

(async () => {
	const raw = new TextDecoder().decode(await Deno.readFile("./roles.toml"));
	const roles = TOML.parse(raw);
	
	for (var role of Object.keys(roles)) {
		if (typeof roles[role].name !== "string") throw new Error("Role names must be strings; got '" + typeof roles[role].name + "'");
		if (typeof roles[role].description !== "string") throw new Error("Role descriptions must be strings; got '" + typeof roles[role].description + "'");
	}

	console.info("All roles passed their tests!");
})();
