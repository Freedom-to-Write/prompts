import { TOML } from "https://taisukef.github.io/toml-es/TOML.js";

(async () => {
  const raw = new TextDecoder().decode(await Deno.readFile("./prompts.toml"));
  const prompts = TOML.parse(raw).prompts;

  for (var prompt of prompts) {
		// Check types
    if (typeof prompt.text !== "string") throw new Error("Prompt text must be a string; got '" + typeof prompt.text + "'");
    if (typeof prompt.updated !== "string") throw new Error("Prompt update date must be a string; got '" + typeof prompt.updated + "' from prompt '" + prompt.text + "'");
		
		// Check if contributors exist
		const raw = new TextDecoder().decode(await Deno.readFile("./contributors.toml"));
		const contributors = TOML.parse(raw);
		if (typeof contributors[prompt.contributor] === 'undefined') throw new Error(`Contributor '${prompt.contributor}' does not exist for prompt '${prompt.text}'`);
  }

	// Check tag types
	for (var tag of prompt.tags) {
		if (typeof tag !== 'string') throw new Error("A tag of prompt '" + prompt.text + "' is not a string.");
	}

  console.info("All prompts passed their tests!");
})();
